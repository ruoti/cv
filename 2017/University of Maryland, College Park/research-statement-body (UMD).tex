
\makestatementheader{Research Statement}
\makefooter{Research Statement}

Security and privacy is an inherently interdisciplinary area, having applications in a wide range of fields---for example, networking, databases, cloud technology, and cyber-physical systems.
It is also strongly connected to human-computer interaction, as user needs define the application-domain and correct user action is often necessary for the system to operate securely.
If user needs, perceptions, and abilities are not taken into account when designing a secure system, it is easy for that system to be secure on paper, but be so unusable that it is insecure in real-world operation~\cite{whitten1999why}.
As such, security and privacy research is most impactful when it encompasses an understanding of both the application-domain and users.

My research is in \textbf{security and privacy}, though I strive to take an interdisciplinary approach that includes \textbf{human-computer interaction (HCI)} and \textbf{usable privacy and security}.
Overall, my research agenda focuses on (1) understanding user perceptions, needs, and abilities in regards to security and privacy, (2) designing and prototyping systems that meet those needs, and (3) verifying that these prototypes achieve their security goals and are sufficiently usable for real-world adoption.
To this end, I employ techniques from traditional security and privacy research---e.g., protocol design, security proofs, threat modeling---as well as from more human-oriented techniques---e.g., usability studies, user interviews, grounded theory. 

In this document, I first describe my dissertation in some detail as it demonstrate the three components of my overall research agenda.
Next, I describe other past and current research projects.
Finally, I describe plans for future research as well as state my commitment to collaboration.

\section{Secure Email for Non-Corporate Users}
My dissertation centered around discovering how to build secure email systems that meet the needs of non-corporate users.
As a first step in this process, I evaluated existing secure email systems, identified flaws with those systems, then prototyped a new secure email system that addressed those flaws~\cite{ruoti2013confused}.
This prototype added end-to-end encryption to existing webmail systems by placing secure interfaces---called \emph{security overlays}---directly on top of the webmail interface allowing users to encrypt and decrypt messages in an isolated context safe from the underlying webmail system.
This approach requires no support from webmail providers and can be generalized to secure a wide range of cloud applications (e.g., social networks, file storage, instant messaging)~\cite{robison2012private,ruoti2017message}.

Next, I conducted a series of usability studies comparing this prototype to other secure email systems.
These studies demonstrated that my research prototype significantly outperformed other available systems, both in terms of usability and in the ability for novice users to successfully send secure email.
Based on the results of these studies, I further refined the research prototype and through another user study identified design principles for building usable, secure email that can be adopted by novice users~\cite{ruoti2016private}.

At this point, I realized that secure email studies had only explored whether users could adopt secure email when paired with an expert coordinator.
While study coordinators played a variety of roles (e.g., a business, the participant's spouse), this did not properly simulate grassroots adoption, wherein pairs of acquaintances adopt secure email between themselves.
To determine whether secure email is ready for grassroots adoption, I developed a novel methodology that recruited pairs of novice acquaintances and tested whether they could collaboratively setup secure email~\cite{ruoti2016we}.
This new methodology revealed that participants had drastically different experiences based on whether they were sending or receiving the first secure email.
Using this methodology, we studied a range of secure email systems, including a modern PGP system, and demonstrated that even after 20 years of usability improvement, PGP-based secure email remains largely unusable for novice users~\cite{ruoti2016why}.
Most recently, we used this methodology to conduct the first non-confounded, A/B evaluation of key management in secure email, exploring public key directories, identity-based encryption, and shared passwords~\cite{ruoti2017message}.

During the course of these studies a consistent theme emerged---participants were interested and open to using secure email in general, but were unsure how often they would need it.
To better explore this question, I conducted a series of interviews exploring user perception towards online risks and their need for secure email~\cite{ruoti2017weighing}.
I then analyzed this data using grounded theory, finding that users were primarily interested in secure email's ability to limit the permanence of their sensitive data online, a concern that was not previously considered in mainstream end-to-end secure email tools.

\section{Past and Current Research}
During my Ph.D. program I collaborated with another Ph.D. student to study TLS proxies---devices that man-in-the-middle TLS communication, both for legitimate (e.g., virus scanning, network filtering) and malicious (e.g., censorship, identity theft) uses.
We first conducted a large-scale Internet measurement of over 15.2 million client devices, finding that roughly 1 in 250 connections had been intercepted by a TLS proxies~\cite{oneill2016tls,oneill2015tls}.
Next, we investigated user attitudes towards TLS proxies using two Amazon Mechanical Turk studies comprising over 2,000 participants; the results of this study showed that users were largely accepting of TLS proxies as long as they were notified and had given their consent~\cite{ruoti2016user}.
Lastly, we built TrustBase, a kernel module that strengthens TLS authentication for all applications without needing to modify those applications~\cite{oneill2017trustbase}, effectively protecting users against malicious TLS proxies.

At MIT Lincoln Laboratory, I am the Principal Investigator on a program exploring Blockchain technology.
The first step for this program was to cut through the hype surround Blockchain technology and determine what it actually is---i.e., what features does it provide that are unavailable elsewhere, what are its limitations, and what uses cases is it best suited for.
As part of this effort, we analyzed over 200 white papers, technical reports, and blogs using grounded theory, allowing us to cleanly separate Blockchain's technological properties (e.g., decentralized governance, audibility, diffused trust) from its ideological goals.
Currently, we are designing and prototyping systems that use Blockchain to store provenance for decentralized systems in a secure and resilient manner.

In my personal time, I collaborate with Kent Seamons on understanding and strengthening user-facing authentication.
As part of this work we evaluate existing authentication systems to identify ways they can be improved~\cite{ruoti2015authentication}.
Most recently, we conducted two studies of YubiKey U2F security keys---the first study was a laboratory study examining device setup and registration with user accounts, the second study was a longitudinal study examining device usage over a period of a month.
We are also currently designing and implementing several systems that strengthen password-based authentication~\cite{smith2017augmenting,ruoti2017end}.

In addition to the above, I am also currently working on a DHS program to create a cybersecurity architecture for all Federal department and agencies. In the past, I have also worked on projects involving unsupervised outlier detection for intrusion prevention~\cite{ruoti2017intrusion} and high-performance, encrypted databases~\cite{ruoti2017pace}.


\section{Future Research}
I am interested in researching a range of security and privacy topics. Below I describe two topics that I anticipate seeking funding for in the near future: strengthening password-based authentication and blockchain-technology in low-connectivity environments.
Currently, I plan to use the first of these topics as the basis of a NSF CAREER proposal.
Both of these topics are interdisciplinary in nature and I look forward to the possibility of collaborating with others at the university to address them in a holistic fashion.

\subsection{Strengthening Password-Based Authentication}
Passwords continue to be an important means for users to authenticate themselves to applications, websites, and backend services.
However, password theft continues to be a significant issue, due in large part to the significant attack surface for passwords, including the operating system (e.g., key loggers), application (e.g., phishing websites in browsers), during transmission (e.g., TLS man-in-the-middle proxies), and at the password verification services (e.g., theft of passwords stored at a server).
Relatedly, even though there is a large body of research on improving passwords, the massive number of application verification services that use passwords stymie the diffusion of improvements---i.e., it does not scale for each improvement to require an update to every application and verification service.

These issues can be addressed through a combination of strong password protocols and safe password entry~\cite{ruoti2016strengthening}.
To implement these two solutions, I propose a new paradigm where most password-related functionality (e.g., entry, management, storage) is transferred to the operating system~\cite{ruoti2017end}.
My research agenda for this project includes fleshing out the design of this new paradigm, prototyping it, and evaluating it to demonstrate that it tangibly strengths password-based authentication.
I'm also interested in exploring research questions related to this new paradigm---for example, 
(1) what are user mental models regarding the security of operating systems,
(2) can we detect legacy password entry interfaces and automatically replace them with this new paradigm,
(3) can we harden the operating system component (e.g., using Intel SGX) to protect against malware.

In particular, I'm intrigued by the ability of this new paradigm to support accessible authentication.
Because all user interaction occurs within the operating system's interface, any accessibility improvements to this core interface will implicitly benefit all applications that use passwords.
I plan to explore whether this interface could be customized on a per-user basis, allowing differently-abled individuals to select interfaces that are best suited to their abilities.

%Finally, I am interested to understand how effectively password managers and password nudges are helping strengthen users passwords.
%In terms of password managers, I would like to run several wide-spread measurements studies to determine how widely deployed password managers are, how users configure them, and how random are the passwords that password managers generate.
%One way I would anticipate doing so it using Amazon Mechanical Turk.
%In regards to password nudges, I'm curious if they actually help users create passwords that resist online and offline attacks.
%Specifically, I'm interested how often password nudges help users resist attacks, and how often they improve password strength, but don't provide any meaningful protection against offline attacks.

\subsection{Blockchain Technology in Low-Connectivity Environments}
Most applications of Blockchain technology require that participants in the system have a consistent connection to the Internet in order to guarantee responsive communication between participants.
I am interested in exploring how Blockchain technology can be adapted to work in low-connectivity environments, where individual participants can only communicate with a small subset of the overall participants.
As a first step, I plan to evaluate existing systems that claim to work in low-connectivity environment to validate whether they achieve their stated functionality and security goals.
Next, I will design, prototype, and evaluate systems that address gaps in the systems I studied.

I am especially interested in using low-connectivity blockchains for two specific applications: (1) information sharing in humanitarian aid and disaster relief (HA/DR) efforts and (2) reliable provenance for autonomous swarms.
HA/DR scenarios are collaborative efforts involving many organizations (e.g., non-governmental organizations (NGO), local government, nation states).
Information sharing between these organizations is crucial to maximize operational effectiveness, but this does not obviate the need for these organization to retain control of their own data.
Unfortunately, first responders operate in low-connectivity environments, preventing them from relying on centralized information services provided by those organizations.
I plan to explore how Blockchain technology can be used to enable peer-to-peer sharing of data between first responders, while still allowing organizations to enforce access control policies on their data and log access attempts from the field.

Similar to first responders, members of autonomous swarms operate in low-connectivity environments, frequently having no connection to the Internet and only a limited connection to members of the swarm that are physically close by.
In conjunction with the fact that not all swarm members will return from a mission, this makes it difficult to have a full history of what happened during the mission.
To address this problem, I plan to leverage Blockchain technology to design and prototype a system that provides resilient provenance storage between swarm members, ensuring that as long as a given fraction of the swarm returns that provenance data will still be available.
I will also explore how Blockchain technology can be used during the mission to provide resilience to swarm members that may be under cyberattack.

%\subsection{Securing Cloud-Based Applications}
%Over the last several years, many organizations have begun moving to the cloud for its lower costs and its increased availability.
%Still, adopting cloud technology often requires that organizations hand over control of their data to the cloud service provider.
%While relying on the cloud provider’s promised security is sufficient for some use cases, it is often insufficient for sensitive data (e.g., personally-identifying information or intellectual property).
%
%In my dissertation, I demonstrated that by placing secure interfaces---called \emph{security overlays}---directly on top of the original interface it is possible to secure webmail without the provider's knowledge or cooperation.
%I plan to investigating how this same technique can be used to secure a range of cloud-based application (i.e., software-as-a-service)~\cite{ruoti2017message}---for example, file storage and online document editing.
%I am especially interested in research how to automatically detect client-side functionality of the original interface and replicate it in the security overlay.
%This research effort will be guided by user studies that identify what security users need for their cloud applications and usability evaluations that demonstrate that the systems we build are sufficiently usable for adoption in users' day-to-day workflows.

\subsection{Usable, Secure Software Engineering}
Developing secure software is inherently difficult, and is further hampered by a rush to market, the lack of cybersecurity-trained architects and developers, and the difficulty of identifying flaws and deploying mitigations.
I believe that this issue is best solved by having applications specify what protections they need, then having the system---and not the developer---decide how to provide those protections~\cite{ruoti2017layering}.
For example, an application would specify that it needs secure network connections and the operating systems would then ensure that all sockets opened by the application were properly encrypted using TLS and that certificates were appropriately validated.
I plan to use techniques form software engineering, HCI, and systems security to build this type of system and ensure that it succeeds at helping developers properly secure their applications.

\section{Collaborations}
I believe that collaborative and interdisciplinary research is inherently more impactful and relevant to real-world problems.
For this reason, I have consistently sought to collaborate with other researchers.
In addition to my collaborations within MIT Lincoln Laboratory, I have active collaborations with Paul van Oorschot (Carleton University), Jeremy Clark (Concordia University), Kent Seamons (Brigham Young University), Daniel Zappala (Brigham Young University), and Ruba Abu-Salma (University College London).
If hired, I will continue striving to establish collaborations on my work both within the university and without.
I am also interested in discovering how my experience in usable privacy and security could benefit other research efforts in the department, college, and university.