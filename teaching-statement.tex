%!TEX encoding = UTF-8 Unicode
\makeatletter\def\input@path{{latex/classes/}{latex/styles/}}\makeatother % Set load paths

\documentclass[statement]{cv}

\title{Teaching Statement}

\begin{document}
	
\maketitle

Teaching and mentoring students are some of the most rewarding parts of being a professor.
For graduate students, I emphasize one-on-one training and mentoring, helping them develop critical problem-solving skills and applying appropriate research methodologies.
Ultimately, my goal is to help these students mature into skilled researchers who can formulate meaningful research questions and answer those questions through the rigorous application of sound scientific principles.
For undergraduate students, my goal is to prepare them for the workforce or graduate school, providing them with the experiences and information necessary to choose between these two career paths.
As part of this, I feel it is essential to involve undergraduates in all stages of my research, helping them gain a deep understanding of the research process and preparing them to continue as graduate students in my research program.

%\section{Previous Experience}
\section{Accomplishments}

At the University of Tennessee, I developed three courses on cybersecurity: applied cryptography, software security, and usable security.
I teach these courses at the senior-level and graduate levels, averaging between 30--50 students each semester.
I consistently receive high student ratings for these classes, averaging 4.8/5.0 across all student evaluation questions.
This is above the median at the departmental, college, and university levels.
These high ratings are in spite of the fact that these courses have extremely challenging projects.
%Furthermore, these high ratings are in spite of the fact that these courses have some of the most challenging projects in the major.

I use a variety of teaching methods to help students succeed.
In the applied cryptography course, I use a flipped classroom model wherein students watch recorded lectures at home. I then use classroom time to discuss questions, to work examples, and to conduct help sessions for homework and projects.
Projects in this class give students in-depth experience using cryptographic primitives and protocols.
Two of these projects have students implement RFCs, which for most is their only experience developing a program against a rigorous specification.

In the software security course, I use a more traditional lecture model.
However, I augment this experience by conducting practicum sessions, where students work together under my supervision to reverse engineer and compromise software programs.
Additionally, the projects for this class give students additional hands-on experience with ethical hacking, introducing them to this skill set and helping them train the proficiency desired by employers.

Finally, the usable security class also uses a traditional lecture model.
Class projects involve students designing and conducting a range of usability studies.
This culminates with a group project where students design and conduct a full user study, including IRB approval.
While these studies are kept small in scale, after the class,  I work with promising students to extend their projects into full research papers.
These student projects have led to two papers published at top security venues.
I expect future projects will lead to many more publications.

I have also had the opportunity to work with and mentor a wide range of students.
I have graduated one Ph.D. student and am currently advising four students.
I have also graduated one MS student with a thesis, 2 MS students with projects in lieu of theses, and 9 course-only students.
I am currently advising four MS students who intend to graduate with an MS thesis.
Additionally, I have had 9 undergraduate researchers work with me and supervised two undergraduate honors theses.

For all of my students, I provide one-on-one training to these students regarding the principles of security and privacy and human-computer interaction.
I also help students build proficiency in the skills needed to conduct research, such as system design, threat modeling, system analysis, study design, participant gathering, study coordination, and result analysis.
This training and mentorship have resulted in eight papers at top venues, including four with undergraduate authors, with more under preparation and submission.
%Furthermore, before my time at the University of Tennessee, I had other opportunities to mentor students, providing twelve students with their first experience authoring a research paper, including seven papers with undergraduate authors.

My student supervision includes targeting students that that come from traditionally underrepresented populations in computing, including women (2 Ph.D. students, 1 MS student, 3 undergraduate students), African American (2 undergraduate students), Hispanic (1 MS student, 1 undergraduate student), and economically impoverished rural students (1 undergraduate students).


\section{Teaching Philosophy}
I believe that students are best served by a mixture of classroom instruction, hands-on experience, and individual mentoring.
As part of this philosophy, I feel that it is essential for students to apply the theoretical concepts they learn in the classroom to solve real-world problems.
Ultimately, education is most successful when it imparts knowledge and practice, allowing students to excel in their future careers.

At the undergraduate level, this translates to a blend of instruction and class projects.
I emphasize class projects as they help students gain a deeper understanding of principles introduced during instruction and help prepare students to translate these principles into practice in the workforce.
While creating quality projects requires additional effort, the payoff is large, especially in helping students value their learning experience.
Furthermore, to help foster a supportive and open learning environment I make lecture slides, study materials, and additional examples available on a class website.
Also, I ensure that I rapidly respond to student communication, allowing students to receive the help and mentoring they might need to succeed in the class.
Finally, I inform students of the exciting research occurring on the topics I am studying, encouraging them to explore undergraduate research and consider graduate education.

At the graduate level, I believe that classes should help students understand how to conduct research in that area.
The curriculum for these classes should include information about the field, suitable research methods, and evaluation techniques.
While it is not necessary for students to complete an entire research project in class, they should ideally have some hands-on experience with the research process in the area being studied.
For example, students could work together to formulate a research question and propose a study that could potentially answer that question.
As students come from various fields, I emphasize collaboration and encourage students to consider how principles from their research areas might apply to the problems studied in class.

Finally, I prioritize the training and mentoring of graduate students.
My goal for each of my students is for them to feel confident in formulating research questions, conducting research to answer those questions, and then articulating their research results in written and spoken form.
To facilitate this goal, I regularly meet with my students to provide personalized, one-on-one mentoring.
At first, students primarily rely on my expertise for conducting their research. However, as they grow, I continue to let them have more control of their research until they ultimately become more like collaborators than students.

%\section{Classes I Can Teach and Develop}
%I am comfortable teaching any class on computer security () and human-computer interaction ().
%I would also be interested in developing courses at the undergraduate and graduate levels on applied cryptography, software security, and usable security.
%Additionally, I would be happy to develop graduate-level special topics courses on authentication, usable encryption, IoT security, reverse engineering, and formal proofs in computer security.
%Additionally, I am confident in teaching any core computer science classes at the XXXX--XXXX levels.
	
\end{document}
