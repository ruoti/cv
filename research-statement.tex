%!TEX encoding = UTF-8 Unicode
\makeatletter\def\input@path{{latex/classes/}{latex/styles/}}\makeatother % Set load paths

\documentclass[statement]{cv}

\title{Research Statement}

\begin{document}
	
\maketitle

There is a significant gap between the theoretical security and privacy properties discussed in academic circles and those available to individuals in their day-to-day lives.
For example, password managers were designed to get users to abandon human-selected passwords with weak security in favor of computer-generated passwords with strong security; however, in practice, due to various usability impediments~\cite{oesch2022observational}, users forego generated passwords, using password managers primarily to store their human-selected passwords~\cite{lyastani2018better,oesch2022observational}.
\emph{In my research, I seek to identify the design principles necessary to bridge the gap between theoretical and practical security and privacy.}

My research approach is multi-faceted.
First, I \emph{conduct empirical user studies} of security and privacy systems, whether academic prototypes or software deployed in the wild, measuring what issues (e.g., usability, functionality, key management requirements) impeded the adoption and correct usage of these systems.
These efforts are enhanced through (a) \emph{collaboration with application-domain experts}, (b) \emph{surveys and interviews of stakeholders}, and (c) \emph{measurement studies of existing deployments}, helping me to more concretely identify key requirements and design constraints for the systems under study.

Next, I rigorously \emph{explore the design space} for these systems, looking for designs with the potential to address the issues previously identified.
These designs can include novel cryptographic protocols and user interfaces, as well as designs improving other aspects of user experience.
I evaluate each of these designs theoretically from both a security (e.g., threat modeling, proofs) and usability (e.g., cognitive walkthroughs, expert review) perspective.

Lastly, for the most promising designs, I \emph{develop proof-of-concept prototypes} and conduct empirical user studies to ensure that these prototypes satisfy stakeholder requirements and identified design constraints under real-world usage.
Critically, these evaluations include both \emph{technical evaluations}---such as measuring latency or demonstrating the ability to stop an attack---and \emph{user-centered evaluations}---such as usability testing and longitudinal studies.
Based on the results of this work, I strive to \emph{extract generalizable design principles} that could help improve the usability, security, and utility of other systems.
%\textbf{Notably, I am one of only a few researchers who takes such a holistic approach to security research}.

Below, I describe my current research agenda, providing an overview of the work I have completed and my plans for future research.


\section{Authentication}

Account breaches have significant negative impacts at the individual, organization, and nation-state levels.
For example, an account breach caused the recent Colonial Pipeline incident, costing the company millions and leading to fuel shortages and panic across the eastern seaboard.
It is well known that a key factor in breaches is weak and reused passwords~\cite{verizon2021data}.
Many authentication systems have been proposed to improve the security of passwords~\cite{bonneau2012quest}. 
However, most of these systems never see real-world usage, and uptake is far from universal when they do.
The \emph{long-term goal} of my research is to (i) identify the impediments to adoption and correct usage of authentication systems and (ii) to design and demonstrate the ability of systems designs to address these impediments, helping users to leverage robust authentication systems to protect their online accounts.

\paragraph{Understanding the Adoption Process}
There is a \emph{critical knowledge gap} regarding the process by which users adopt authentication systems, preventing system designs from addressing this process and thereby decreasing the likelihood of supplanting traditional password-based authentication.
To address this gap, my overall objective is to model the adoption process for authentication systems, enabling system designs that address users' needs throughout this process.
The rationale for this research is that designing authentication systems to support the adoption process will improve the security and usability of those systems and increase the likelihood that they can supplant passwords.

I began my investigation of this area by conducting a laboratory study investigating the setup process for YubiKey and a empirical study observing users in their first two weeks using a YubiKey~\cite{reynolds2018tale}.
These studies revealed significant problems with the setup process for hardware security tokens. However, these studies also demonstrated that users generally enjoyed day-to-day usage.
I have also conducted observational interviews of password manager users, having them demonstrate and explain how they configure their password manager, create accounts (including credential selection/generation), log into accounts, and update accounts.
Results from this study strongly suggest a multi-phase process by which users had adopted their manager, with phases including setup, acclimation, gradual replacement of existing passwords with generated passwords, and steady-state usage.

These studies demonstrated a need for longitudinal studies to flesh out the adoption process model for authentication systems.
To this end, I plan to conduct large-scale (200+~participant), long-term (1-year), longitudinal studies, observing users as they adopt an authentication tool into their daily lives.
Initially, I will conduct studies for hardware security tokens (e.g., a YubiKey) and password managers, using the collected quantitative and qualitative data to model the adoption process for each type of tool.
Next, I will compare these models to derive a standard model describing the adoption process of authentication systems or demonstrate that it is not possible to create a unified model.
Finally, I will design, prototype, and evaluate adoption process-aware systems, measuring their ability to improve security, utility, and usability during adoption and day-to-day usage.
This research plan is \emph{creative and original} in that it breaks from the status quo of treating adoption as a binary process---users do or do not adopt the tool---instead modeling it as a process consisting of distinct phases, each with their respective behaviors and transitions.

\paragraph{Exploring Security and Usability Designs}
Even when adopted, \emph{security-critical functionality of new authentication systems is underutilized}~\cite{lyastani2018better}.
To address this issue, my overall objective is to identify design principles that can improve the security, utility, and usability of authentication systems.
Moreover, I will quantify and explain the ability of competing system designs to encourage correct usage of these systems.
The rationale for this research is that it will increase the understanding of underlying issues, identify generalizable design principles, and promote the design of more usable and secure password managers.

I have already evaluated the security of over twenty password managers on desktop~\cite{oesch2020that} and mobile systems~\cite{oesch2021emperors}, examining the full password manager lifecycle---password generation, storage, and autofill.
My results identified numerous security issues with these browsers. One of the most damaging and persistent is the ability of malicious apps, websites, and browser extensions to steal credentials as they are autofilled.
To address this issue, I am currently exploring how to build a trusted pathway into the browser, allowing for credentials to be transmitted safely.
I plan to extend this research, exploring other ways browser-supported authentication can improve the security and usability of authentication systems~\cite{ruoti2017end}.

I have also conducted usability studies of various web authentication systems~\cite{ruoti2015authentication}, including an in-depth systematization of the use cases design paradigms for password managers~\cite{simmons2021systematization}.
Based on this research, I have identified the following research plan to increase the utility, utilization, and usability of password managers.
First, I will quantify the ability of entry-aware password generation---generating passwords that are easy to enter on devices where the password manager is unavailable---to increase users' willingness to use generated passwords.
Second, I will quantify the extent to which prioritizing suggestions from password health checks improves users' compliance with those suggested actions.
Third, I will describe the unique processes, needs, and challenges faced by parents and children using password managers and quantify the extent to which system designs addressing this reality improve usability and security.
This research plan is \emph{creative and original} in that it breaks from the status quo of high-level usability assessments, instead quantifying and explaining the ability of system designs to address specific usability issues and improve the utilization of security features.
Furthermore, it will be the first research investigating password health checks and multi-user password manager usage.

\section{Key Management}

Users are constantly sharing vast amounts of information over digital channels, with much of this information being sensitive~\cite{ruoti2017weighing,oesch2020understanding}.
However, existing communication systems have insufficient security and privacy protections~\cite{unger2015sok,clark2021sok}, enabling widespread surveillance of Internet traffic by governments and data mining by cloud service providers.
As such, there is \emph{a critical need} for systems that enable users to take control of their online data, deciding when and by whom it can be accessed.
While the cryptographic primitives necessary to secure online communication and data storage are well known~\cite{unger2015sok,clark2021sok} the key challenge lies in designing systems that are sufficiently usable to ensure secure and correct usage by non-expert users~\cite{ruoti2019johnnys}.

In my research, I have conducted large-scale surveys and interviews of email~\cite{ruoti2017weighing} and group chat~\cite{oesch2020understanding,oesch2021user} users to understand their requirements for secure online communication.
I have also conducted technical and usability evaluations of the security and usability primitives found in modern secure messaging tools~\cite{ruoti2016were,ruoti2019usability,clark2021sok}.
To address identified issues, I created and evaluated various design patterns to improve usability and real-world security.
These design principles include seamless integration of secure email systems within existing communication tools~\cite{robison2012private,ruoti2013confused}, inline, guided tutorials~\cite{ruoti2016private}, delayed encryption~\cite{ruoti2016private}, and manual encryption~\cite{ruoti2013confused,ruoti2016private}.
I also conducted the first A/B study comparing the usability of key management schemes~\cite{ruoti2018comparative} (identity-based encryption, PGP, and shared passwords) and measured the usability of secure email based on short-lived secrets~\cite{monson2018usability}.
\emph{This research resulted in the first secure email system shown to be usable by non-expert users}, with user testing demonstrating that it had better usability and security outcomes than any other research or enterprise systems~\cite{ruoti2016were,ruoti2016private,ruoti2019usability}.
My system also generalizes to secure other forms of online communication and cloud storage~\cite{ruoti2018comparative}.

With this significant progress, my attention has now turned to the \emph{knowledge gap} regarding usable key management.
While my work has demonstrated how to help users manage a single key over a short time, it remains unclear how key management systems designs can (i) help users manage a large number of keys (ii) over a long time and (iii) without a centralized key management service.
Such knowledge is needed to enable the real-world deployment of advanced cryptographic schemes that rely on users to manage a large number of keys over their lifetimes.
As the first step in this direction, I am investigating how businesses using S/MIME to encrypt their email can enable cross-organization key discovery.
I also plan to investigate challenges users face during key synchronization and recovery, then create and quantify the ability of system designs to address these challenges.
This research plan is \emph{creative and original} in that it breaks from the status quo of considering only short-term and centralized key management, instead considering the need for long-term management of keys in a decentralized environment.

\section{Internet of Things (IoT) Security}

IoT devices are ever-present in modern life, with over 70\% of homes having at least one device~\cite{kumar2019all}.
Unfortunately, these devices are riddled with security vulnerabilities, such as hard-coded credentials and cryptographic key material, poorly encrypted communications, and weak access control.
Existing research has focused on correcting implementation flaws in these devices. However, the history of software development suggests that while improvement will occur, it is unlikely that we will ever reach a point where all, or even a majority of developers, properly implement necessary security protections.
As such, there is an \emph{urgent need} to identify and evaluate methods for securing IoT devices without requiring modification of those devices or the services they rely upon.

I propose addressing this need by designing add-on security devices that are inserted into home networks to ensure secure operation from IoT devices on that network.
While a similar approach has already been successfully used in industry~\cite{ruoti2017layering}---for example, using zero-trust networking gateways to enforce strong authentication---research is needed to evaluate this approach's feasibility, strengths, and weaknesses in resource-constrained home networks.
In this research, I will quantify the ability of these devices to (i) ensure properly encrypted network communication, including upgrading plaintext channels, enforcing correct certificate validation, and rejecting unsafe cipher specs; (ii) enforcing strong authentication for IoT devices using a zero-trust model; and (iii) filtering malicious traffic including denial of service attacks.
I will also investigate different places these add-on devices can be injected into the network, such as between the IoT device and the router, at the router, or before the router.
Additionally, I will conduct user studies of developed systems to ensure that users can adequately configure these devices to provide a secure network.
Finally, I will collaborate with computer engineering faculty to engineering these devices to be physically small and have minimal power draw, allowing them to be used in a wide variety of situations.

As the first step in this direction, I have demonstrated that correct TLS certificate validation can be enforced for applications running using a modified Linux kernel~\cite{oneill2017trustbase,ruoti2017layering}.
I have also built an IoT testbed and demonstrated that correct certificate validation is enforcable by inserting an add-on device between the IoT devices and the router.
I am in the process of expanding the functionality of this prototype device and exploring the effectiveness of this technique at other locations in the network.


\footnotesize
\bibliographystyle{acm}
\bibliography{latex/bibtex/publications,latex/bibtex/authentication,latex/bibtex/usable-encryption,latex/bibtex/iot}
	
\end{document}
